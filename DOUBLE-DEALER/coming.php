<!doctype html>
<html lang="de-CH">

	<head>
		<meta charset="uft-8">
		<title>COMING SOON | DOUBLE-DEALER</title>
		<link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.0/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-9ndCyUaIbzAi2FUVXJi0CjmCapSmO7SnpJef0486qhLnuZ2cdeRhO02iuK6FUUVM" crossorigin="anonymous">
		<link rel="stylesheet" href="assets/css/styles.css" type="text/css">
		<link rel="stylesheet" media="screen" href="https://fontlibrary.org//face/better-eb-garamond" type="text/css"/>
        <link rel="stylesheet" media="screen" href="https://fontlibrary.org//face/silverblade" type="text/css"/>
	</head>
	
	<body>
		<header>
			<p>DOUBLE-DEALER
		</header>
		
        
		<?php
            // navbar
            require_once(__DIR__.'/nav.php');

        ?>
		
		<main>
			<h1>Coming Soon</h1>
            <div class="center"><img class="homepic" src="assets/images/Hello.jpg" alt="cover" style="display: block; margin-left: auto; margin-right: auto; width: 50%;"></div>
			
		</main>
		
		<footer>
			<div class="text-center p-3"><p>
				&copy; 2023, DOUBLE-DEALER </p>
				
			  </div>
		</footer>
	</body>
	
</html>