<!doctype html>
<html lang="de-CH">

	<head>
		<meta charset="uft-8">
		<title>IMPRESSUM | DOUBLE-DEALER</title>
		<link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.0/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-9ndCyUaIbzAi2FUVXJi0CjmCapSmO7SnpJef0486qhLnuZ2cdeRhO02iuK6FUUVM" crossorigin="anonymous">
		<link rel="stylesheet" href="assets/css/styles.css" type="text/css">
        <link rel="stylesheet" href="assets/css/impstyle.css" type="text/css">
		<link rel="stylesheet" media="screen" href="https://fontlibrary.org//face/better-eb-garamond" type="text/css"/>
		<link rel="stylesheet" media="screen" href="https://fontlibrary.org//face/silverblade" type="text/css"/>
		<link rel="stylesheet" media="screen" href="https://fontlibrary.org//face/bellota" type="text/css"/>	
		<link rel="stylesheet" media="screen" href="assets/css/webfont/cinzel/stylesheet.css" type="text/css"/>	
	</head>
	
	<body>
		<header>
			<p>DOUBLE-DEALER
		</header>
		
        
		<?php
            // navbar
            require_once(__DIR__.'/nav.php');

        ?>
	
		<main>
			<h1>Impressum</h1>
			
			<div id="notes">
				<article>
                    <p id="title">Web-site</p>
                    <p id="name">Vinuja Vivekanantharasa </p>
                    <p id="addr">Romanshorn 8590 <br>
                    Centralstrasse 14<br>
                    vinuja.vivekanantharasa@edu.tbz.ch</p>
                </article>
				
				<article>
                    <p id="title">Inhalt</p>
                    <p id="name">Jane Kuttikadan</p>
                    <p id="addr">Dürnten 8635<br>
                    Etzelstrasse 5<br>
                    jane.kuttikadan@edu.tbz.ch</p>
                </article>
				
				<article>
                    <p id="title"> Manga Bilder </p>
                    <p id="name">Vinuja Vivekanantharasa</p>
                    <p id="addr">Romanshorn 8590<br>
                    Centralstrasse 14<br>
                    vinuja.vivekanantharasa@edu.tbz.ch</p>
                </article>
			
			</div>	
			
		</main>
		
		<footer>
			<div class="text-center p-3"><p>
				&copy; 2023, DOUBLE-DEALER </p>
				
			  </div>
		</footer>
	</body>
	
</html>