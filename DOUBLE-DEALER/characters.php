<!doctype html>
<html lang="de-CH">

	<head>
		<meta charset="uft-8">
		<title>CHARCTERS | DOUBLE-DEALER</title>
		<link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.0/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-9ndCyUaIbzAi2FUVXJi0CjmCapSmO7SnpJef0486qhLnuZ2cdeRhO02iuK6FUUVM" crossorigin="anonymous">

		<link rel="stylesheet" href="assets/css/styles.css" type="text/css">
		<link rel="stylesheet" href="assets/css/charstyle.css" type="text/css">
		<link rel="stylesheet" media="screen" href="https://fontlibrary.org//face/better-eb-garamond" type="text/css"/>
        <link rel="stylesheet" media="screen" href="https://fontlibrary.org//face/silverblade" type="text/css"/>
		<link rel="stylesheet" href="https://fonts.googleapis.com/css2?family=Material+Symbols+Outlined:opsz,wght,FILL,GRAD@48,400,0,0" />
		<link rel="preconnect" href="https://fonts.googleapis.com">
		<link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
		<link href="https://fonts.googleapis.com/css2?family=Catamaran:wght@100&family=Indie+Flower&display=swap" rel="stylesheet">
		<link rel="stylesheet" media="screen" href="https://fontlibrary.org//face/bellota" type="text/css"/>	

	</head>
	
	<body>
		<header>
			<p>DOUBLE-DEALER</p>
		</header>
		
        
		<?php
            // navbar
            require_once(__DIR__.'/nav.php');

        ?>
		
		<main>
			<h1>Characters</h1>

			<div id = "format">
				<nav id = "charas">
				<p class="cast">Cast</p>
				<ul class="list-group">
					<li class="list-group-item">					
						<span class="material-symbols-outlined">group</span>
						<a href="characters.php"> Jalebi </a>
					<li class="list-group-item">
						<span class="material-symbols-outlined">group</span>
						<a href="vadai.php"> Vadai</a>
				</ul>
            </nav>

			<article id = "desc">

					<div id = "show">
						<div id = "stat">
							<p id="name">Jalebi</p>
							<p id="personality"> "Professional" Thief </p>
						</div>
					
					
						<img id = "char" src="assets/images/jalebi.png" alt="character named Jalebi">

					</div>

            </article>

			</div>
			
		</main>
		
		<footer>
			<div class="text-center p-3"><p>
				&copy; 2023, DOUBLE-DEALER </p>
				
			  </div>
		</footer>
	
		</body>
	</body>
	
</html>