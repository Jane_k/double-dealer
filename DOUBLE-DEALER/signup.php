<!doctype html>
<html lang="de-CH">

	<head>
		<meta charset="uft-8">
		<title>SIGN UP | DOUBLE-DEALER</title>
		<link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.0/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-9ndCyUaIbzAi2FUVXJi0CjmCapSmO7SnpJef0486qhLnuZ2cdeRhO02iuK6FUUVM" crossorigin="anonymous">
		<link rel="stylesheet" href="assets/css/styles.css" type="text/css">
		<link rel="stylesheet" href="assets/css/forumstyle.css" type="text/css">
		<link rel="stylesheet" href="assets/css/loginstyle.css" type="text/css">
		<link rel="stylesheet" media="screen" href="https://fontlibrary.org//face/better-eb-garamond" type="text/css"/>
		<link rel="stylesheet" media="screen" href="https://fontlibrary.org//face/silverblade" type="text/css"/>
		<link rel="stylesheet" media="screen" href="https://fontlibrary.org//face/bellota" type="text/css"/>	
		<link rel="stylesheet" media="screen" href="assets/css/webfont/cinzel/stylesheet.css" type="text/css"/>	
		<link rel="stylesheet" href="https://fonts.googleapis.com/css2?family=Material+Symbols+Outlined:opsz,wght,FILL,GRAD@48,400,0,0" />
	</head>
	
	<body>
		<header>
			<p>DOUBLE-DEALER
		</header>
		
        
		<?php
            // navbar
            require_once(__DIR__.'/nav.php');

        ?>
	
		<main>
			<h1>Forum - Sign Up</h1>
            <article id = "log">
                <div id="formlog">
					<form accept-charset="utf-8" action="insertuser.php" method="post" id="enterlog">

							<label for="username" id="top">Username</label>
							<input type="text" placeholder="Enter Username" name="uname" id="username" required>
							<label for="psw">Password</label>
							<input type="password" placeholder="Enter Password" name="psw" id="psw" required>

							<input class ="list-group-item" id = "logbutton" type="Submit"  value="Sign Up">

                        </form>
                    </div>
            </article>			
		</main>
		
		<footer>
			<div class="text-center p-3"><p>
				&copy; 2023, DOUBLE-DEALER </p>
				
			  </div>
		</footer>
	</body>
	
</html>