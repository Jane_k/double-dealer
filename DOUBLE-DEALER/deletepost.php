<?php

session_start();

require 'vendor/autoload.php';

$client = new MongoDB\Client("mongodb://localhost:27017/");

$doubledealer = $client->doubledealer;
$postcollection = $doubledealer->post;
 
?>

<!doctype html>
<html lang="de-CH">

<head>
<meta charset="uft-8">
		<title>CREATE POST | DOUBLE-DEALER</title>
		<link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.0/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-9ndCyUaIbzAi2FUVXJi0CjmCapSmO7SnpJef0486qhLnuZ2cdeRhO02iuK6FUUVM" crossorigin="anonymous">
		<link rel="stylesheet" href="assets/css/styles.css" type="text/css">
		<link rel="stylesheet" href="assets/css/forumstyle.css" type="text/css">
		<link rel="stylesheet" href="assets/css/poststyle.css" type="text/css">
		<link rel="stylesheet" media="screen" href="https://fontlibrary.org//face/better-eb-garamond" type="text/css"/>
		<link rel="stylesheet" media="screen" href="https://fontlibrary.org//face/silverblade" type="text/css"/>
		<link rel="stylesheet" media="screen" href="https://fontlibrary.org//face/bellota" type="text/css"/>	
		<link rel="stylesheet" media="screen" href="assets/css/webfont/cinzel/stylesheet.css" type="text/css"/>	
		<link rel="stylesheet" href="https://fonts.googleapis.com/css2?family=Material+Symbols+Outlined:opsz,wght,FILL,GRAD@48,400,0,0" />
</head>

<body>
    <header>
        <p>DOUBLE-DEALER
    </header>

    <nav id="mainnav">
        <ul class="nav nav-pills nav-justified">
            <li class="nav-item">
                <a class="nav-link" href="index.html">Home</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="characters.html">Characters</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="manga.html">Manga</a>
            </li>
            <li class="nav-item dropdown">
                <a class="nav-link dropdown-toggle" data-bs-toggle="dropdown" href="#" role="button"
                    aria-expanded="false">About Us</a>
                <ul class="dropdown-menu">
                    <li><a class="dropdown-item" href="about.html">About Us</a></li>
                    <li><a class="dropdown-item" href="impressum.html">Company Details</a></li>
                </ul>
            </li>
            <li class="nav-item dropdown">
                <a class="nav-link dropdown-toggle" data-bs-toggle="dropdown" href="#" role="button"
                    aria-expanded="false">Coming Soon</a>
                <ul class="dropdown-menu">
                    <li><a class="dropdown-item" href="coming.html">Coming Soon</a></li>
                    <li><a class="dropdown-item" href="forum.php">Forum</a></li>
                </ul>
            </li>
        </ul>
    </nav>


    <!-- PHP CODE MongoDB -->
    <?php

        //get cleansed values
        $id = htmlspecialchars($_POST['postId']);

        $post = $postcollection->findOne(
            ['_id' => new MongoDB\BSON\ObjectId($id)]
        );

        $deleteResult = $postcollection->deleteOne(['_id' => new MongoDB\BSON\ObjectId($id)]);

        ?>

    <main>
        <h1>Forum - Delete Post</h1>
        <article id = "log">
             <div id="postbox">
				<form accept-charset="utf-8" action="forum.php" method="post" id="enterlog">

					<input class ="list-group-item" id = "logbutton" type="Submit"  value="Back to Forum">

                </form>

            </div>
        </article>
    </main>

    <footer>
        <div class="text-center p-3">
            <p>
                &copy; 2023, DOUBLE-DEALER </p>

        </div>
    </footer>


</body>

</html>
