<!doctype html>
<html lang="de-CH">

	<head>
		<meta charset="uft-8">
		<title>ABOUT | DOUBLE-DEALER</title>
		<link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.0/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-9ndCyUaIbzAi2FUVXJi0CjmCapSmO7SnpJef0486qhLnuZ2cdeRhO02iuK6FUUVM" crossorigin="anonymous">
		<link rel="stylesheet" href="assets/css/styles.css" type="text/css">
		<link rel="stylesheet" media="screen" href="https://fontlibrary.org//face/better-eb-garamond" type="text/css"/>
        <link rel="stylesheet" media="screen" href="https://fontlibrary.org//face/silverblade" type="text/css"/>
		<link rel="stylesheet" media="screen" href="https://fontlibrary.org//face/bellota" type="text/css"/>	
		<link rel="stylesheet" href="assets/css/aboutstyle.css" type="text/css"/>
	</head>
	
	<body>
		<header>
			<p>DOUBLE-DEALER
		</header>
		
        
		<?php
            // navbar
            require_once(__DIR__.'/nav.php');

        ?>
		
		<main>
			<h1>About Us</h1>
			
			<div id="us">
				<article id="person">
					<div id = "aut">
						<p id="artist">Bloxb</p>
						<p id="stats">"Let me cook"</p>
					</div>
					<img id="profile" src="assets/images/bloxb.jpeg" alt="artist's icon of bloxb">
				</article>
				
				<article id = "person">
					<div id = "aut">
						<p id="artist">Artem</p>
						<p id="stats"> "Modern problems require <br>
						modern solutions" 
					</div>
					<img id="profile" src="assets/images/artem.png" alt="artist's icon of artem">
				</article>
				
			
			</div>	
			
		</main>
		
		<footer>
			<div class="text-center p-3"><p>
				&copy; 2023, DOUBLE-DEALER </p>
				
			  </div>
		</footer>
	</body>
	
</html>