<?php
session_start();
require 'vendor/autoload.php';

try {
	$client = new MongoDB\Client("mongodb://localhost:27017/");
	$doubledealer = $client->doubledealer;
	$postcollection = $doubledealer->post;
} catch (\Throwable $th) {
	echo "currently not available";
}	

?>


<!doctype html>
<html lang="de-CH">

	<head>
		<meta charset="uft-8">
		<title>FORUM | DOUBLE-DEALER</title>
		<link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.0/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-9ndCyUaIbzAi2FUVXJi0CjmCapSmO7SnpJef0486qhLnuZ2cdeRhO02iuK6FUUVM" crossorigin="anonymous">
		<link rel="stylesheet" href="assets/css/styles.css" type="text/css">
		<link rel="stylesheet" href="assets/css/forumstyle.css" type="text/css">
		<link rel="stylesheet" media="screen" href="https://fontlibrary.org//face/better-eb-garamond" type="text/css"/>
		<link rel="stylesheet" media="screen" href="https://fontlibrary.org//face/silverblade" type="text/css"/>
		<link rel="stylesheet" media="screen" href="https://fontlibrary.org//face/bellota" type="text/css"/>	
		<link rel="stylesheet" media="screen" href="assets/css/webfont/cinzel/stylesheet.css" type="text/css"/>	
		<link rel="stylesheet" href="https://fonts.googleapis.com/css2?family=Material+Symbols+Outlined:opsz,wght,FILL,GRAD@48,400,0,0" />
	</head>
	
	<body>
		<header>
			<p>DOUBLE-DEALER
		</header>
		
        
		<?php
            // navbar
            require_once(__DIR__.'/nav.php');

        ?>
	
		<main>
			<h1>Forum</h1>
			<div id = "format">
            <nav id = "topic">
				<p class="topics">Topics</p>
				<ul class="list-group">
					<li class="list-group-item">					
						<span class="material-symbols-outlined">group</span>
						<a href="#"> General </a>
					<li class="list-group-item">
						<span class="material-symbols-outlined">group</span>
						<a href="#"> Dev Updates</a>
					<li class="list-group-item">						
						<span class="material-symbols-outlined">group</span>
						<a href="#"> Story </a>
					<li class="list-group-item">
						<span class="material-symbols-outlined">group</span>
						<a href="#"> Reviews </a>
					<li class="list-group-item">
						<span class="material-symbols-outlined">group</span>
						<a href="#"> Memes </a>
				</ul>
            </nav>
            <article id = "forum">
				<div id = "topicname">
					<img src = "assets/images/outline_group_white_48dp.png" id="group" alt="Group Icon">
					<p>General</p>
				</div>
				<div id="postlist">
					<div id="admintool">
					<div id = "post">
						<img src = "assets/images/profile.png" id ="user" alt="Profile Picture">
						<div id = "text">
							<p id = "title"> My first Post </p>
							<p id = "content">This is my post :3 </p>
							<p id = "author"> by Artem </p>
						</div>
					</div>

					<?php
						if (isset($_SESSION['admin']) && $_SESSION['admin']) {
					?>
						<form accept-charset="utf-8" action="createpost.html" method="post">
							<input class ="list-group-item" id = "delbtn" type="submit"  value="Delete">
						</form>
					<?php
						}
					?>
					</div>

					<div id="admintool">
					<div id = "post">
						<img src = "assets/images/profile.png" id ="user" alt="Profile Picture">
						<div id = "text">
							<p id = "title"> Hello World </p>
							<p id = "content">My first post :0 </p>
							<p id = "author"> by Bloxb </p>
						</div>
					</div>
					<?php
						if (isset($_SESSION['admin']) && $_SESSION['admin']) {
					?>
						<form accept-charset="utf-8" action="createpost.html" method="post">
							<input class ="list-group-item" id = "delbtn" type="submit"  value="Delete">
						</form>
					<?php
						}
					?>
					</div>

					<?php
			    		$postlist = $postcollection->find();

						//generate post html
						foreach($postlist as $post){
							
							$title = $post['title'];
							$content = $post['content'];
							$author = $post['author'];
							$id = $post['_id'];
					?>

					<div id="admintool">
					<div id = "post">
						<img src = "assets/images/profile.png" id ="user" alt="Profile Picture">
						<div id = "text">
							<p id = "title"><?php echo $title ?></p>
							<p id = "content"><?php echo $content ?></p>
							<p id = "author"><?php echo $author ?></p>
						</div>
					</div>
					<?php
						if (isset($_SESSION['admin']) && $_SESSION['admin']) {
					?>
						<form accept-charset="utf-8" action="deletepost.php" method="post">
							<!-- puts id of post -->
							<input type="hidden" id="postId" name="postId" value=<?php echo $id ?>>
							<input class ="list-group-item" id = "delbtn" type="submit"  value="Delete">
						</form>
					<?php
						}
					?>
					</div>

					<?php
						}
					?>

				</div>
            </article>
            <article id="login">

			<?php
			//show login content
			if (!isset($_SESSION['name'])) {
			?>

                <p>
                    Log in to interact with our community!
                </p>
				<!--change the list pls, cuz it's looking bad-->
				<ul class="list-group">
					<form accept-charset="utf-8" action="login.php" method="post">
						<input class ="list-group-item" id = "btn" type="submit"  value="Log In">
					</form>
					<form accept-charset="utf-8" action="signup.php" method="post">
						<input class ="list-group-item" id = "btn"  type="submit" value="Sign Up">
					</form>
				</ul>

			<?php
			//show create post content
			} else {
			?>

			<p>
				Share your Thoughts with our community!
			</p>
			<!--change the list pls, cuz it's looking bad-->
			<ul class="list-group">
				<form accept-charset="utf-8" action="createpost.html" method="post">
					<input class ="list-group-item" id = "btn" type="submit"  value="Create Post">
				</form>
				<form accept-charset="utf-8" action="logout.php" method="post">
					<input class ="list-group-item" id = "btn"  type="submit" value="Log Out">
				</form>
			</ul>

			<?php
			}
			?>


            </article>
			</div>	
			
			
		</main>
		
		<footer>
			<div class="text-center p-3"><p>
				&copy; 2023, DOUBLE-DEALER </p>
				
			  </div>
		</footer>
	</body>
	
</html>