    <nav id ="mainnav">
			<ul class="nav nav-pills nav-justified">
				<li class="nav-item">
				  <a class="nav-link" href="index.php">Home</a>
				</li>
				<li class="nav-item">
				  <a class="nav-link" href="characters.php">Characters</a>
				</li>
				<li class="nav-item">
				  <a class="nav-link" href="manga.php">Manga</a>
				</li>
				<li class="nav-item dropdown">
					<a class="nav-link dropdown-toggle" data-bs-toggle="dropdown" href="#" role="button" aria-expanded="false">About Us</a>
					<ul class="dropdown-menu">
					  <li><a class="dropdown-item" href="about.php">The Artists</a></li>
					  <li><a class="dropdown-item" href="impressum.php">Company Details</a></li>
					</ul>
				  </li>
				  <li class="nav-item dropdown">
					<a class="nav-link dropdown-toggle" data-bs-toggle="dropdown" href="#" role="button" aria-expanded="false">Coming Soon</a>
					<ul class="dropdown-menu">
					  <li><a class="dropdown-item" href="coming.php">Coming Soon</a></li>
					  <li><a class="dropdown-item" href="forum.php">Forum</a></li>
					</ul>
				  </li>
			  </ul>
		</nav>
