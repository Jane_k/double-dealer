<!doctype html>
<html lang="de-CH">

	<head>
		<meta charset="uft-8">
		<title>CREATE POST | DOUBLE-DEALER</title>
		<link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.0/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-9ndCyUaIbzAi2FUVXJi0CjmCapSmO7SnpJef0486qhLnuZ2cdeRhO02iuK6FUUVM" crossorigin="anonymous">
		<link rel="stylesheet" href="assets/css/styles.css" type="text/css">
		<link rel="stylesheet" href="assets/css/forumstyle.css" type="text/css">
		<link rel="stylesheet" href="assets/css/poststyle.css" type="text/css">
		<link rel="stylesheet" media="screen" href="https://fontlibrary.org//face/better-eb-garamond" type="text/css"/>
		<link rel="stylesheet" media="screen" href="https://fontlibrary.org//face/silverblade" type="text/css"/>
		<link rel="stylesheet" media="screen" href="https://fontlibrary.org//face/bellota" type="text/css"/>	
		<link rel="stylesheet" media="screen" href="assets/css/webfont/cinzel/stylesheet.css" type="text/css"/>	
		<link rel="stylesheet" href="https://fonts.googleapis.com/css2?family=Material+Symbols+Outlined:opsz,wght,FILL,GRAD@48,400,0,0" />
	</head>
	
	<body>
		<header>
			<p>DOUBLE-DEALER
		</header>
		
        
		<?php
            // navbar
            require_once(__DIR__.'/nav.php');

        ?>
	
		<main>
			<h1>Forum - Create Post</h1>
            <article id = "log">
                <div id="postbox">
					<form accept-charset="utf-8" action="insertpost.php" method="post" id="enterlog">

							<label for="title" id="top">Title</label>
							<input type="text" placeholder="Write your Title" name="title" id="username" required>
							<label for="post_content">Write your thoughts</label>
							<textarea name="post_content" id="post_content" rows="5" cols="30" required> </textarea>

							<input class ="list-group-item" id = "logbutton" type="Submit"  value="Post">

                        </form>

                    </div>


            </article>			
		</main>
		
		<footer>
			<div class="text-center p-3"><p>
				&copy; 2023, DOUBLE-DEALER </p>
				
			</div>
		</footer>
	</body>
	
</html>