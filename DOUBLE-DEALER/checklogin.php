<?php
session_start();
require 'vendor/autoload.php';

$client = new MongoDB\Client("mongodb://localhost:27017/");

$doubledealer = $client->doubledealer;
$usercollection = $doubledealer->user;
?>

<!doctype html>
<html lang="de-CH">

<head>
    <meta charset="uft-8">
    <title>LOG IN | DOUBLE-DEALER</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.0/dist/css/bootstrap.min.css" rel="stylesheet"
        integrity="sha384-9ndCyUaIbzAi2FUVXJi0CjmCapSmO7SnpJef0486qhLnuZ2cdeRhO02iuK6FUUVM" crossorigin="anonymous">
    <link rel="stylesheet" href="assets/css/styles.css" type="text/css">
    <link rel="stylesheet" href="assets/css/forumstyle.css" type="text/css">
    <link rel="stylesheet" href="assets/css/loginstyle.css" type="text/css">
    <link rel="stylesheet" media="screen" href="https://fontlibrary.org//face/better-eb-garamond" type="text/css" />
    <link rel="stylesheet" media="screen" href="https://fontlibrary.org//face/silverblade" type="text/css" />
    <link rel="stylesheet" media="screen" href="https://fontlibrary.org//face/bellota" type="text/css" />
    <link rel="stylesheet" media="screen" href="assets/css/webfont/cinzel/stylesheet.css" type="text/css" />
    <link rel="stylesheet"
        href="https://fonts.googleapis.com/css2?family=Material+Symbols+Outlined:opsz,wght,FILL,GRAD@48,400,0,0" />
</head>

<body>
    <header>
        <p>DOUBLE-DEALER
    </header>
		
        
		<?php
            // navbar
            require_once(__DIR__.'/nav.php');

        ?>

    <!-- PHP CODE -->
    <?php
        $name = htmlspecialchars($_POST['uname']);
        $pw = htmlspecialchars($_POST['psw']);

        //checks if username exists & match password
        $userdocument = $usercollection->count(
            ['username' => $name, 'password' => $pw]
        );



        if($userdocument == 1){

            $useradmin = $usercollection->findOne(
                ['username' => $name, 'password' => $pw]
            );
    
            $admin = $useradmin->admin;
    
            // Session saves the name after successful login
            $_SESSION["name"]= $name;
            $_SESSION["admin"]= $admin;
            header("Location: success.html");
        } else{
    ?>

    <main>
        <h1>Forum - Login</h1>
        <article id="log">
            <div id="formlog">
                <form accept-charset="utf-8" action="checklogin.php" method="post" id="enterlog">

                    <label for="uname" id="top">Username</label>
                    <input type="text" placeholder="Enter Username" name="uname" required>
                    <label for="psw">Password</label>
                    <input type="password" placeholder="Enter Password" name="psw" required>

                    <input class="list-group-item" id="logbutton" type="Submit" value="Log In">

                </form>
                <p> Username or Password is wrong. Please try again           
            </div>
        </article>
    </main>

    <?php
        }
    ?>

    <footer>
        <div class="text-center p-3">
            <p>
                &copy; 2023, DOUBLE-DEALER </p>
        </div>
    </footer>


</body>

</html>