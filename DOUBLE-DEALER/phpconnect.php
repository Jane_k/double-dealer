<?php
session_start(); // Session
?>
<!doctype html>
<html lang="de-CH">

<head>
    <meta charset="uft-8">
    <title>LOG IN | DOUBLE-DEALER</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.0/dist/css/bootstrap.min.css" rel="stylesheet"
        integrity="sha384-9ndCyUaIbzAi2FUVXJi0CjmCapSmO7SnpJef0486qhLnuZ2cdeRhO02iuK6FUUVM" crossorigin="anonymous">
    <link rel="stylesheet" href="assets/css/styles.css" type="text/css">
    <link rel="stylesheet" href="assets/css/forumstyle.css" type="text/css">
    <link rel="stylesheet" href="assets/css/loginstyle.css" type="text/css">
    <link rel="stylesheet" media="screen" href="https://fontlibrary.org//face/better-eb-garamond" type="text/css" />
    <link rel="stylesheet" media="screen" href="https://fontlibrary.org//face/silverblade" type="text/css" />
    <link rel="stylesheet" media="screen" href="https://fontlibrary.org//face/bellota" type="text/css" />
    <link rel="stylesheet" media="screen" href="assets/css/webfont/cinzel/stylesheet.css" type="text/css" />
    <link rel="stylesheet"
        href="https://fonts.googleapis.com/css2?family=Material+Symbols+Outlined:opsz,wght,FILL,GRAD@48,400,0,0" />
</head>

<body>
    <header>
        <p>DOUBLE-DEALER
    </header>

    <nav id="mainnav">
        <ul class="nav nav-pills nav-justified">
            <li class="nav-item">
                <a class="nav-link" href="index.html">Home</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="characters.html">Characters</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="manga.html">Manga</a>
            </li>
            <li class="nav-item dropdown">
                <a class="nav-link dropdown-toggle" data-bs-toggle="dropdown" href="#" role="button"
                    aria-expanded="false">About Us</a>
                <ul class="dropdown-menu">
                    <li><a class="dropdown-item" href="about.html">About Us</a></li>
                    <li><a class="dropdown-item" href="impressum.html">Company Details</a></li>
                </ul>
            </li>
            <li class="nav-item dropdown">
                <a class="nav-link dropdown-toggle" data-bs-toggle="dropdown" href="#" role="button"
                    aria-expanded="false">Coming Soon</a>
                <ul class="dropdown-menu">
                    <li><a class="dropdown-item" href="coming.html">Coming Soon</a></li>
                    <li><a class="dropdown-item" href="forum.html">Forum</a></li>
                </ul>
            </li>
        </ul>
    </nav>

    <!-- PHP CODE -->
    <?php
            $loginstatus = false;
            $name = htmlspecialchars($_POST['uname']);
            $pw = htmlspecialchars($_POST['psw']);
            
            // php - MySQL connection
            include('db_inc.php');
            $dsn = 'mysql:host=' . $host . ';dbname=' . $database;
            $options = [PDO::MYSQL_ATTR_INIT_COMMAND => 'SET NAMES utf8'];
            include('connect.php');

            $db -> setAttribute(PDO::ATTR_DEFAULT_FETCH_MODE, PDO::FETCH_ASSOC);

            $query = "SELECT * FROM benutzer";
            $result = $db -> query($query);

            $resarr = $result -> fetchAll();
            $result = NULL;
            $db = NULL;
            
            $counter = 0;
            $arrayName = array();
            $arrayPW = array();

            foreach($resarr as $row){
                $counter = 0;
                foreach($row as $val){
                    switch($counter){
                        case 1: 
                            $arrayName[] = $val;
                            break;
                        case 2:
                            $arrayPW[] = $val;
                            break;
                        default:
                            break;    
                    }      
                    $counter = $counter + 1;

                }
            }

            if (in_array($name, $arrayName) && in_array($pw, $arrayPW)) {
                // Session for Login 
                $_SESSION["email"]= $name; // Wert speichern
                $loginstatus = true;
                header("location: forum.html");
                exit;

            };
            

        ?>

    <main>
        <h1>Forum - Login</h1>
        <article id="log">
            <div id="formlog">
                <form accept-charset="utf-8" action="phpconnect.php" method="post" id="enterlog">

                    <label for="uname" id="top">Username</label>
                    <input type="text" placeholder="Enter Username" name="uname" required>
                    <label for="psw">Password</label>
                    <input type="password" placeholder="Enter Password" name="psw" required>

                    <input class="list-group-item" id="logbutton" type="Submit" value="Log In">

                </form>
                <p> <?php 
                if($loginstatus) {?>
                Login successfully
                <?php }else{ ?>
                Username or Password were wrong, try again
                <?php } ?>

                
                
            </div>
        </article>
    </main>

    <footer>
        <div class="text-center p-3">
            <p>
                &copy; 2023, DOUBLE-DEALER </p>

        </div>
    </footer>


</body>

</html>