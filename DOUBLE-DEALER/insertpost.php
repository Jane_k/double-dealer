<?php

session_start();

require 'vendor/autoload.php';

$client = new MongoDB\Client("mongodb://localhost:27017/");

$doubledealer = $client->doubledealer;
$postcollection = $doubledealer->post;
 
?>

<!doctype html>
<html lang="de-CH">

<head>
<meta charset="uft-8">
		<title>CREATE POST | DOUBLE-DEALER</title>
		<link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.0/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-9ndCyUaIbzAi2FUVXJi0CjmCapSmO7SnpJef0486qhLnuZ2cdeRhO02iuK6FUUVM" crossorigin="anonymous">
		<link rel="stylesheet" href="assets/css/styles.css" type="text/css">
		<link rel="stylesheet" href="assets/css/forumstyle.css" type="text/css">
		<link rel="stylesheet" href="assets/css/poststyle.css" type="text/css">
		<link rel="stylesheet" media="screen" href="https://fontlibrary.org//face/better-eb-garamond" type="text/css"/>
		<link rel="stylesheet" media="screen" href="https://fontlibrary.org//face/silverblade" type="text/css"/>
		<link rel="stylesheet" media="screen" href="https://fontlibrary.org//face/bellota" type="text/css"/>	
		<link rel="stylesheet" media="screen" href="assets/css/webfont/cinzel/stylesheet.css" type="text/css"/>	
		<link rel="stylesheet" href="https://fonts.googleapis.com/css2?family=Material+Symbols+Outlined:opsz,wght,FILL,GRAD@48,400,0,0" />
</head>

<body>
    <header>
        <p>DOUBLE-DEALER
    </header>
		
        
		<?php
            // navbar
            require_once(__DIR__.'/nav.php');

        ?>


    <!-- PHP CODE MongoDB -->
    <?php

        //get cleansed values
        $title = htmlspecialchars($_POST['title']);
        $content = htmlspecialchars($_POST['post_content']);
        $status = true;
        $user = $_SESSION['name'];


            //insert record + user needs to be taken from session
            $insertOneResult = $postcollection->insertOne(
                ['title' => $title
               , 'content' => $content
               ,'author' => $user]
            );

        ?>

    <main>
        <h1>Forum - Sign Up</h1>
        <article id = "log">
             <div id="postbox">
				<form accept-charset="utf-8" action="forum.php" method="post" id="enterlog">

					<input class ="list-group-item" id = "logbutton" type="Submit"  value="Back to Forum">

                </form>

            </div>
        </article>
    </main>

    <footer>
        <div class="text-center p-3">
            <p>
                &copy; 2023, DOUBLE-DEALER </p>

        </div>
    </footer>


</body>

</html>
